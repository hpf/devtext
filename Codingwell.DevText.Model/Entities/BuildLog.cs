using System;
using System.Collections.Generic;

namespace Codingwell.DevText.Model.Entities
{
	public class BuildLog
	{
		public int ID { get; set; }
		public string Title { get; set; }
		public string LogContent { get; set; }
		public System.DateTime InsertTime { get; set; }
		public int RankIndex { get; set; }
		public int State { get; set; }
	}
}

